const carouselItems = document.querySelectorAll(".carousel-item");
const customerNavContainer = document.querySelector(".customer--nav-container");
const customerIconHTML = `<div class="customer--index-icon"></div>`;
const indicatorContainer = document.querySelector(".carousel-indicators");

// Preload default indicator toggle
carouselItems.forEach((e, i) => {
  indicatorContainer.insertAdjacentHTML(
    "beforeend",
    `<button type="button" id="indicators-${i}" data-bs-target="#carouselExampleSlidesOnly"
    data-bs-slide-to="${i}" ${
      i === 0 ? `class="active"` : ""
    } aria-current="true" aria-label="Slide ${i++}"></button>`
  );
});

carouselItems.forEach((e) => {
  customerNavContainer.insertAdjacentHTML("afterbegin", customerIconHTML);
});

const customerNavIcon = document.querySelectorAll(".customer--index-icon");
customerNavIcon[0].classList.add("active");
carouselItems[0].classList.add("active");

const removeAllActive = () => {
  customerNavIcon.forEach((e) => {
    e.classList.remove("active");
  });
};

customerNavIcon.forEach((e, i) => {
  e.addEventListener("click", () => {
    removeAllActive();
    e.classList.add("active");
    document.getElementById(`indicators-${i}`).click();
  });
});
